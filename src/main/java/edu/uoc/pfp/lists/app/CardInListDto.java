package edu.uoc.pfp.lists.app;

import edu.uoc.pfp.lists.domain.Card;
import edu.uoc.pfp.lists.domain.Color;
import edu.uoc.pfp.lists.domain.Id;
import edu.uoc.pfp.lists.domain.TaskCompletion;

import org.jetbrains.annotations.NotNull;

import java.util.Set;

public class CardInListDto {

    @NotNull
    public final Id id;

    @NotNull
    public final String name;

    @NotNull
    public final Set<Color> colors;

    @NotNull
    public TaskCompletion taskCompletion;


    public CardInListDto(@NotNull Id id, @NotNull String name, @NotNull Set<Color> colors) {
        this.id = id;
        this.name = name;
        this.colors = colors;
        taskCompletion = new TaskCompletion(0,0);
    }

    public static CardInListDto fromCard(Card c){
        return new CardInListDto(c.getId(), c.getName(), c.getColors());
    }
}
