package edu.uoc.pfp.lists.app;

import edu.uoc.pfp.lists.domain.Card;
import edu.uoc.pfp.lists.domain.List;
import org.jetbrains.annotations.NotNull;

import java.util.LinkedList;

public class ListDto {

    @NotNull
    public final String name;

    @NotNull
    public final java.util.List<CardInListDto> cards;

    public ListDto(@NotNull String name, @NotNull java.util.List<CardInListDto> cards) {
        this.name = name;
        this.cards = cards;
    }

    public static ListDto fromList(List l) {
        java.util.List<CardInListDto> cards = new LinkedList<CardInListDto>();
        for(Card c  : l.getCards()){
            cards.add(CardInListDto.fromCard(c));
        }
        return new ListDto(l.getName(), cards);
    }
}
