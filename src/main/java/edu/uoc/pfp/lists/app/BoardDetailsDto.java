package edu.uoc.pfp.lists.app;

import edu.uoc.pfp.lists.domain.Board;
import edu.uoc.pfp.lists.domain.BoardVisibility;
import edu.uoc.pfp.lists.domain.Id;
import edu.uoc.pfp.lists.domain.List;
import org.jetbrains.annotations.NotNull;

import java.util.LinkedList;

public class BoardDetailsDto {

    @NotNull
    public final Id id;

    @NotNull
    public final String name;

    @NotNull
    public final BoardVisibility visibility;

    @NotNull
    public final java.util.List<ListDto> lists;

    private BoardDetailsDto(@NotNull Id id, @NotNull String name, @NotNull BoardVisibility visibility, @NotNull java.util.List<ListDto> lists) {
        this.id = id;
        this.name = name;
        this.visibility = visibility;
        this.lists = lists;
    }

    public static BoardDetailsDto fromBoard(Board b) {
        java.util.List<ListDto> lists = new LinkedList<ListDto>();
        for(List l : b.getLists()){
            lists.add(ListDto.fromList(l));
        }
        return new BoardDetailsDto(b.getId(), b.getName(),b.getVisibility(), lists);
    }
}
