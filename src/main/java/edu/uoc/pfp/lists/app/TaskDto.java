package edu.uoc.pfp.lists.app;

public class TaskDto {
    public final String name;
    public final boolean done;

    public TaskDto(String name, boolean done) {
        this.name = name;
        this.done = done;
    }
}
