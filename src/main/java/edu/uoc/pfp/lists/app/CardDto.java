package edu.uoc.pfp.lists.app;

import edu.uoc.pfp.lists.domain.Card;
import edu.uoc.pfp.lists.domain.Color;
import edu.uoc.pfp.lists.domain.Id;
import org.jetbrains.annotations.NotNull;

import java.util.LinkedList;
import java.util.Set;

public class CardDto extends CardInListDto{

    public final String description;

    public java.util.List<ChecklistDto> checklists;

    public CardDto(@NotNull Id id, @NotNull String name, @NotNull Set<Color> colors, String description) {
        super(id, name, colors);
        this.description = description;
        this.checklists = new LinkedList<ChecklistDto>();
    }

    public static CardDto fromCard(Card card){
        return new CardDto(card.getId(),card.getName(),card.getColors(), card.getDescription());
    }
}
