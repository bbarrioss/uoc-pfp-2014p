package edu.uoc.pfp.lists.app;

import java.util.LinkedList;

public class ChecklistDto {

    public final String name;
    public java.util.List<TaskDto> tasks;

    public ChecklistDto(String name) {
        this.name = name;
        this.tasks = new LinkedList<TaskDto>();
    }
}
