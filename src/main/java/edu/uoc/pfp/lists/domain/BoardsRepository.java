package edu.uoc.pfp.lists.domain;

import org.jetbrains.annotations.NotNull;

public interface BoardsRepository {

    void save(Board board);

    @NotNull
    Board getBoard(Id id);
}
