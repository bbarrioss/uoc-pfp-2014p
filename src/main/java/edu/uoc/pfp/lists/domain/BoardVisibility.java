package edu.uoc.pfp.lists.domain;

public enum BoardVisibility {
    publicBoard, privateBoard
}
