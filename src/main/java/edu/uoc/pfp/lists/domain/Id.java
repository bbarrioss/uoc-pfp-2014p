package edu.uoc.pfp.lists.domain;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class Id {

    @NotNull
    private final String id;

    public Id(){
        this.id = IdUtils.newId();
    }

    public Id(@NotNull String id) {
        this.id = id;
    }

    @NotNull
    @Override
    public String toString() {
        return id;
    }

    @Override
    public boolean equals(@Nullable Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Id that = (Id) o;

        return id.equals(that.id);

    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }
}
