package edu.uoc.pfp.lists.domain;

import edu.uoc.pfp.lists.app.BoardDetailsDto;
import edu.uoc.pfp.lists.app.CardDto;
import edu.uoc.pfp.lists.app.ListsService;
import edu.uoc.pfp.util.Utils;
import org.jetbrains.annotations.NotNull;

public class ListsServiceImpl implements ListsService {


    @NotNull
    private BoardsRepository boardsRepository;

    public ListsServiceImpl(@NotNull BoardsRepository boardsRepository) {
        this.boardsRepository = boardsRepository;
    }


    @Override
    public Id newBoard(@NotNull String boardName, @NotNull BoardVisibility boardVisibility) {
        Board b = new Board(boardName,boardVisibility);
        b.addList("To Do");
        b.addList("Doing");
        b.addList("Done");
        boardsRepository.save(b);
        return b.getId();
    }

    @Override
    public BoardDetailsDto getBoard(Id id) {
        Board b = boardsRepository.getBoard(id);
        if(b == null) return null;
        return BoardDetailsDto.fromBoard(b);
    }

    @Override
    public void renameBoard(Id id, String newName) {
        Board b = boardsRepository.getBoard(id);
        b.rename(newName);
        boardsRepository.save(b);
    }

    @Override
    public void addList(Id boardId, String listName) {
        Board b = boardsRepository.getBoard(boardId);
        b.addList(listName);
    }

    @Override
    public void renameList(Id boardId, String currentListName, String newListName) {
        Board b = boardsRepository.getBoard(boardId);
        b.renameList(currentListName,newListName);
    }

    @Override
    public Id addCard(Id boardId, String listName, String cardName) {
        Board b = boardsRepository.getBoard(boardId);
        return b.addCard(listName, cardName);
    }

    @Override
    public void addCardColor(Id boardId, Id cardId, Color color) {
        Board b = boardsRepository.getBoard(boardId);
        b.addCardColor(cardId, color);
    }

    @Override
    public void editCardDescription(Id boardId, Id cardId, String newDescription) {
        Board b = boardsRepository.getBoard(boardId);
        b.editCardDescription(cardId,newDescription);
    }

    @Override
    public CardDto getCardDetails(Id boardId, Id cardId) {
        Board b = boardsRepository.getBoard(boardId);
        return b.getCardDetails(cardId);
    }

    @Override
    public void moveCard(Id boardId, Id cardId, String destinationListName, int position) {
        Board b = boardsRepository.getBoard(boardId);
        b.moveCard(cardId, destinationListName, position);
    }

    @Override
    public Id addChecklist(Id boardId, Id cardId, String checklistName) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Id addTask(Id boardId, Id checklistId, String taskName) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void markTaskDone(Id boardId, Id taskId) {
        throw new UnsupportedOperationException();
    }

}
