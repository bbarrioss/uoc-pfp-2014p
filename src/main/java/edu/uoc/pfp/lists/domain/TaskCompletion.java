package edu.uoc.pfp.lists.domain;

public class TaskCompletion {

    public final int completedTasks;

    public final int totalNumberOfTasks;


    public TaskCompletion(int completedTasks, int totalNumberOfTasks) {
        this.completedTasks = completedTasks;
        this.totalNumberOfTasks = totalNumberOfTasks;
    }
}
