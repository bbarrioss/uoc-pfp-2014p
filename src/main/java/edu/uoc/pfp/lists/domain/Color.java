package edu.uoc.pfp.lists.domain;

public enum Color {
    green, yellow, orange, red, purple, blue
}
