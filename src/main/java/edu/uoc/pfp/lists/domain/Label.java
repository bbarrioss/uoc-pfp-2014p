package edu.uoc.pfp.lists.domain;

import org.jetbrains.annotations.NotNull;

import java.util.LinkedList;

public class Label {

    @NotNull
    private Color color;

    private String name;

    Label(@NotNull Color color){
        this.color = color;
    }

    static java.util.List<Label> createLabelSet(){
        java.util.List<Label> result = new LinkedList<Label>();
        for(Color c : Color.class.getEnumConstants()){
            result.add(new Label(c));
        }
        return result;
    }

    @NotNull
    public Color getColor() {
        return color;
    }

    public String getName() {
        return name;
    }
}
