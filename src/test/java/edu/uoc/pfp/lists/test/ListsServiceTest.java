package edu.uoc.pfp.lists.test;

import edu.uoc.pfp.lists.app.*;
import edu.uoc.pfp.lists.dal.InMemoryBoardsRepository;
import edu.uoc.pfp.lists.domain.*;
import edu.uoc.pfp.util.Utils;
import org.jetbrains.annotations.NotNull;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class ListsServiceTest {

    @NotNull
    private ListsService listsService;

    @NotNull
    private Id sampleBoardId;

    @Before
    public void init(){
        BoardsRepository boardsRepository = new InMemoryBoardsRepository();
        listsService = new ListsServiceImpl(boardsRepository);
        sampleBoardId = listsService.newBoard("Board 1", BoardVisibility.privateBoard);
    }

    @Test
    public void testNewBoard(){
        Id id = listsService.newBoard("Sample board", BoardVisibility.privateBoard);
        BoardDetailsDto boardDetails = listsService.getBoard(id);
        assertEquals("Sample board", boardDetails.name);
        assertEquals(BoardVisibility.privateBoard,boardDetails.visibility);
        assertEquals(3,boardDetails.lists.size());
        assertEquals("To Do", boardDetails.lists.get(0).name);
        assertEquals(Utils.<CardInListDto>list(), boardDetails.lists.get(0).cards);
        assertEquals("Doing", boardDetails.lists.get(1).name);
        assertEquals(Utils.<CardInListDto>list(), boardDetails.lists.get(1).cards);
        assertEquals("Done", boardDetails.lists.get(2).name);
        assertEquals(Utils.<CardInListDto>list(), boardDetails.lists.get(2).cards);
    }

    @Test
    public void testRenameBoard(){
        listsService.renameBoard(sampleBoardId, "New name");
        BoardDetailsDto boardDetails = listsService.getBoard(sampleBoardId);
        assertEquals("New name", boardDetails.name);
    }

    @Test
    public void testAddList(){
        listsService.addList(sampleBoardId, "New list");
        BoardDetailsDto boardDetails = listsService.getBoard(sampleBoardId);
        assertEquals(4,boardDetails.lists.size());
        assertEquals("New list", boardDetails.lists.get(3).name);
        assertEquals(Utils.<CardInListDto>list(), boardDetails.lists.get(3).cards);
    }

    @Test
    public void testRenameList(){
        listsService.renameList(sampleBoardId, "To Do", "Backlog");
        BoardDetailsDto boardDetails = listsService.getBoard(sampleBoardId);
        assertEquals("Backlog",boardDetails.lists.get(0).name);
    }

    @Test
    public void testAddCardToList(){
        listsService.addCard(sampleBoardId,"To Do","Sample card");
        List<CardInListDto> toDoCards = listsService.getBoard(sampleBoardId).lists.get(0).cards;
        assertEquals(1,toDoCards.size());
        CardInListDto newCard = toDoCards.get(0);
        assertEquals("Sample card",newCard.name);
        assertEquals(Utils.<Color>set(),newCard.colors);
    }

    @Test
    public void testEditCardDescription(){
        Id sampleCardId = listsService.addCard(sampleBoardId,"To Do","Sample card");
        listsService.editCardDescription(sampleBoardId, sampleCardId, "New description");
        CardDto sampleCard = listsService.getCardDetails(sampleBoardId, sampleCardId);
        assertEquals("New description", sampleCard.description);
    }

    @Test
    public void testMoveCard(){
        Id sampleCardId = listsService.addCard(sampleBoardId,"To Do","Sample card");
        listsService.moveCard(sampleBoardId,sampleCardId,"Doing",1);
        List<CardInListDto> toDoCards = listsService.getBoard(sampleBoardId).lists.get(0).cards;
        assertEquals(0, toDoCards.size());
        List<CardInListDto> doneCards = listsService.getBoard(sampleBoardId).lists.get(1).cards;
        assertEquals(1, doneCards.size());
        assertEquals(sampleCardId, doneCards.get(0).id);
    }

    @Test
    public void tesAddCardColor(){
        Id sampleCardId = listsService.addCard(sampleBoardId,"To Do","Sample card");
        listsService.addCardColor(sampleBoardId, sampleCardId, Color.green);
        CardInListDto greenCard = listsService.getBoard(sampleBoardId).lists.get(0).cards.get(0);
        assertEquals(Utils.set(Color.green), greenCard.colors);
    }

    @Test
    public void testAddChecklist(){
        Id sampleCardId = listsService.addCard(sampleBoardId,"To Do","Sample card");
        listsService.addChecklist(sampleBoardId,sampleCardId,"Tasks");
        CardDto cardDetails = listsService.getCardDetails(sampleBoardId, sampleCardId);
        ChecklistDto checklist = cardDetails.checklists.get(0);
        assertEquals("Tasks", checklist.name);
        assertEquals(0, checklist.tasks.size());
    }

    @Test
    public void testAddTask(){
        Id sampleCardId = listsService.addCard(sampleBoardId,"To Do","Sample card");
        Id sampleChecklistId = listsService.addChecklist(sampleBoardId, sampleCardId, "Tasks");
        listsService.addTask(sampleBoardId,sampleChecklistId,"Sample task");
        CardDto cardDetails = listsService.getCardDetails(sampleBoardId, sampleCardId);
        ChecklistDto checklist = cardDetails.checklists.get(0);
        assertEquals(1,checklist.tasks.size());
        TaskDto task = checklist.tasks.get(0);
        assertEquals("Sample task",task.name);
        assertEquals(false,task.done);
    }

    @Test
    public void testMarkTaskDone(){
        Id sampleCardId = listsService.addCard(sampleBoardId,"To Do","Sample card");
        Id sampleChecklistId = listsService.addChecklist(sampleBoardId, sampleCardId, "Tasks");
        Id sampleTaskId = listsService.addTask(sampleBoardId,sampleChecklistId,"a");
        listsService.markTaskDone(sampleBoardId, sampleTaskId);
        CardDto cardDetails = listsService.getCardDetails(sampleBoardId, sampleCardId);
        TaskDto task = cardDetails.checklists.get(0).tasks.get(0);
        assertEquals(true, task.done);
    }

    @Test
    public void testTasksCounters(){
        Id sampleCardId = listsService.addCard(sampleBoardId,"To Do","Sample card");
        Id checklist1 = listsService.addChecklist(sampleBoardId,sampleCardId,"Tasks");
        Id checklist2 = listsService.addChecklist(sampleBoardId,sampleCardId,"Impediments");
        Id taskAId = listsService.addTask(sampleBoardId,checklist1,"a");
        listsService.addTask(sampleBoardId,checklist1,"b");
        listsService.addTask(sampleBoardId,checklist1,"c");
        Id task1Id = listsService.addTask(sampleBoardId,checklist2,"1");
        listsService.addTask(sampleBoardId,checklist2,"2");
        listsService.markTaskDone(sampleBoardId, taskAId);
        listsService.markTaskDone(sampleBoardId, task1Id);
        CardInListDto sampleCard = listsService.getBoard(sampleBoardId).lists.get(0).cards.get(0);
        TaskCompletion tc = sampleCard.taskCompletion;
        assertEquals(2, tc.completedTasks);
        assertEquals(5, tc.totalNumberOfTasks);
        
        TareasFaltantes tc2 = new TareasFaltantes(tc.totalNumberOfTasks,tc.completedTasks); 
        int resultado = tc2.getTareasFaltantes(); 
        System.out.println ("tareas no completadas: " + resultado ); 

    }


}
