package edu.uoc.pfp.lists.domain;

import edu.uoc.pfp.lists.domain.*;
import edu.uoc.pfp.util.Utils;
import junit.framework.TestCase;
import org.jetbrains.annotations.NotNull;

import static edu.uoc.pfp.util.Utils.*;

public class CardTest extends TestCase{

    @NotNull
    private Board b = new Board("SampleBoard", BoardVisibility.privateBoard);
    @NotNull
    private List l = b.addList("SampleList");

    public void testEditDescription(){
        Card c = l.addCard(new Id("1"), "SampleCard");
        c.editDescription("New description");
        assertEquals("New description", c.getDescription());
    }

    public void testMoveInsideList(){
        Card c1 = l.addCard(new Id("1"),"c1");
        Card c2 = l.addCard(new Id("2"),"c2");
        Card c3 = l.addCard(new Id("3"),"c3");
        Card c4 = l.addCard(new Id("4"),"c4");
        c1.move(l,1);
        assertEquals(list(c1, c2, c3, c4), l.getCards());
        c1.move(l, 2);
        assertEquals(list(c2, c1, c3, c4), l.getCards());
        c1.move(l, 1);
        assertEquals(list(c1, c2, c3, c4), l.getCards());
    }

    public void testMoveToDifferentList(){
        List l2 = b.addList("l2");
        Card c11 = l.addCard(new Id("11"),"c11");
        Card c12 = l.addCard(new Id("12"),"c12");
        Card c21 = l2.addCard(new Id("21"),"c21");
        Card c22 = l2.addCard(new Id("22"),"c22");
        c11.move(l2,1);
        assertEquals(list(c11, c21, c22), l2.getCards());
        assertEquals(list(c12), l.getCards());
        c12.move(l2, 3);
        assertEquals(list(c11, c21, c12, c22), l2.getCards());
        assertEquals(Utils.<Card>list(), l.getCards());
    }
}
