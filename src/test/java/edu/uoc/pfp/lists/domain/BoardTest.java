package edu.uoc.pfp.lists.domain;

import edu.uoc.pfp.lists.domain.*;
import junit.framework.TestCase;

import static edu.uoc.pfp.util.Utils.*;

import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.Set;

public class BoardTest extends TestCase {

    public void testBoardConstruction(){
        Board b = new Board("TestBoard", BoardVisibility.privateBoard);
        assertEquals("TestBoard", b.getName());
        assertEquals(BoardVisibility.privateBoard,b.getVisibility());
        assertEquals(new LinkedList<List>(),b.getLists());
        assertNotNull(b.getId());
        assertEquals(6, b.getLabels().size());
        Set<Color> labelColors = new LinkedHashSet<Color>();
        for(Label l : b.getLabels()){
            assert(l.getName() == null);
            labelColors.add(l.getColor());
        }
        assertEquals(6,labelColors.size());
    }

    public void testBoardRename(){
        Board b = new Board("TestBoard", BoardVisibility.privateBoard);
        b.rename("newName");
        assertEquals("newName", b.getName());
    }

    public void testNewList(){
        Board b = new Board("TestBoard", BoardVisibility.privateBoard);
        List l = b.addList("SampleList");
        assertEquals("SampleList",l.getName());
        assertEquals(new LinkedList<Card>(),l.getCards());
        assertEquals(list(l),b.getLists());
    }


}
